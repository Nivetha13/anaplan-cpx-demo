import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatTableDataSource } from '@angular/material';
import { Location } from '@angular/common';
import { SelectionModel } from '@angular/cdk/collections';
import { QuotaSyncService } from './service/QuotaSyncService';

export interface PeriodicElement {
  Account_Name: string;
  Status:string;
  Type: string;
  Started:string;
  Duration:string;
  Actions:string;
}
const ELEMENT_DATA: PeriodicElement[] = [
  {Account_Name:'Quota', Status: 'Pending', Type: 'Data Sync', Started: 'Dec 11, 2013 at 6.01 PM', Duration:'00.00.00', Actions:" "}
];


@Component({
  selector: 'app-anaplan-jobs',
  templateUrl: './anaplan-jobs.component.html',
  styleUrls: ['./anaplan-jobs.component.css']
})
export class AnaplanJobsComponent implements OnInit {
    [x: string]: any;
    displayedColumns: string[] = ['Account_Name', 'Status', 'Type', 'Started', 'Duration', 'Actions'];
    dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
    isDataProcessing = true;
    startTime = null;
    endTime = null;
    constructor(private location: Location, private readonly quotaSyncService:QuotaSyncService){}
  
    goBack(){
      this.location.back();
    }
  
    updateRowStatus(acName,colName,colValue){
      this.dataSource.data.map((item)=>{
        if(item["Account_Name"] == acName ){
          item[colName] = colValue;
        }
      })
    }
  @ViewChild(MatSort) sort: MatSort;
  
  ngOnInit() {
    this.isDataProcessing = true;
    this.dataSource.sort = this.sort;
    this.startTime = new Date();
  this.updateRowStatus("Quota","Started",this.startTime.toString());
  this.quotaSyncService.getAll({})
  .subscribe(response => {
    if(response.stateMessage == null){
      //success 
      this.endTime = new Date();
      this.isDataProcessing = false;
      this.updateRowStatus("Quota","Status","Success");
      this.updateRowStatus("Quota","Duration",((this.endTime - this.startTime)/1000).toFixed(2)+ " s");
    }else{
      //failed
      this.updateRowStatus("Quota","Status","Success");
    }
  });
  } 
  }

