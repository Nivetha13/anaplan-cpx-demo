import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnaplanJobsComponent } from './anaplan-jobs.component';

describe('AnaplanJobsComponent', () => {
  let component: AnaplanJobsComponent;
  let fixture: ComponentFixture<AnaplanJobsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnaplanJobsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnaplanJobsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
