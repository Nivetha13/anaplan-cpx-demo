import { Location } from '@angular/common';
import { Component, ViewChild } from '@angular/core';
import { MatSort, MatTableDataSource } from '@angular/material';
import { AccountSyncService } from './service/AccountSyncService';
import { RevenueSyncService } from './service/RevenueSyncService';

export interface PeriodicElement {
  Account_Name: string;
  Status:string;
  Type: string;
  Started:string;
  Duration:string;
  Actions:string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {Account_Name:'Account', Status: 'Pending', Type: 'Data Sync', Started: 'Jun 07, 2019 at 6.00 PM', Duration:'00.00.00', Actions:" "},
  {Account_Name:'Revenue', Status: 'Pending', Type: 'Data Sync', Started: 'Jun 07, 2019 at 6.00 PM', Duration:'00.00.00', Actions:" "},
];

@Component({
  selector: 'app-jobs',
  templateUrl: './jobs.component.html',
  styleUrls: ['./jobs.component.css']
})
export class JobsComponent {
  [x: string]: any;
  displayedColumns: string[] = ['Account_Name', 'Status', 'Type', 'Started', 'Duration', 'Actions'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);
  isDataProcessing = true;
  startTime = null;
  endTime = null;
  constructor(private location: Location,private readonly accountSyncService: AccountSyncService,private readonly revenueSyncService: RevenueSyncService){}

  goBack(){
    this.location.back();
  }

  updateRowStatus(acName,colName,colValue){
    this.dataSource.data.map((item)=>{
      if(item["Account_Name"] == acName ){
        item[colName] = colValue;
      }
    })
  }

@ViewChild(MatSort) sort: MatSort;

doRevenueSync(){
  this.startTime = new Date();
  this.updateRowStatus("Revenue","Started",this.startTime.toString());
  this.revenueSyncService.getAll({})
  .subscribe(response => {
    if(response.dumpFiles == null){
      //success
      this.endTime = new Date();
      this.isDataProcessing = false;
      this.updateRowStatus("Revenue","Status","Success");
      this.updateRowStatus("Revenue","Duration",((this.endTime - this.startTime)/1000).toFixed(2)+ " s");
    }else{
      //failed
      this.updateRowStatus("Revenue","Status","Success");
    }
  });
}

ngOnInit() {
  this.isDataProcessing = true;
  this.dataSource.sort = this.sort;
  this.startTime = new Date();
  this.updateRowStatus("Account","Started",this.startTime.toString());
  this.accountSyncService.getAll({})
  .subscribe(response => {
    if(response.dumpFiles == null){
      //success 
      this.endTime = new Date();
      this.updateRowStatus("Account","Status","Success");
      this.updateRowStatus("Account","Duration",((this.endTime - this.startTime)/1000).toFixed(2)+ " s");
     this.doRevenueSync();
    }else{
      //failed
      this.updateRowStatus("Account","Status","Success");
      this.doRevenueSync();
    }
  });
}
}

